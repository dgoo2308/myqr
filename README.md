# myqr

Python program to generate my personal QR-codes


## Code


```bash
pip3 install qrcode[pil]
```

## Code 

run with

```bash
python3 myqr
```

## Output

|Linkedin| Gitlab|
|---|---|
|![](img/Danny_Goossen_LinkedIn_QR.png)| ![](img/Danny_Goossen_Gitlab_QR.png)|
|![GitHub QR](img/Danny_Goossen_Github_QR.png)| ![Gioxa QR](img/Danny_Goossen_Gioxa_QR.png)|
