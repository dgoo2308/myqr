
#!/usr/bin/env python3
import qrcode

from qrcode.image.styledpil import StyledPilImage
from qrcode.image.styles.moduledrawers import RoundedModuleDrawer
from qrcode.image.styles.colormasks import RadialGradiantColorMask

qr= qrcode.QRCode(error_correction=qrcode.constants.ERROR_CORRECT_H)

qr.add_data('https://gitlab.com/dgoo2308')

img_1 = qr.make_image(image_factory= StyledPilImage, 
                          module_drawer=RoundedModuleDrawer(),
                          color_mask=RadialGradiantColorMask(edge_color=(255,131,49),center_color=(0,0,0)),
                          embeded_image_path="Logos/gitlab-logo.png")

img_1.save("img/Danny_Goossen_Gitlab_QR.png")

qr= qrcode.QRCode(error_correction=qrcode.constants.ERROR_CORRECT_H)
qr.add_data('https://www.linkedin.com/in/dgoo2308/')

img_2 = qr.make_image(image_factory= StyledPilImage, module_drawer=RoundedModuleDrawer(),
                          color_mask=RadialGradiantColorMask(edge_color=(23,129,185),center_color=(0,0,0)),
                          embeded_image_path="Logos/linkedin-logo.png")

img_2.save("img/Danny_Goossen_LinkedIn_QR.png")


qr= qrcode.QRCode(error_correction=qrcode.constants.ERROR_CORRECT_H)
qr.add_data('https://github.com/dgoo2308')

img_2 = qr.make_image(image_factory= StyledPilImage, module_drawer=RoundedModuleDrawer(),
                          color_mask=RadialGradiantColorMask(edge_color=(118,48,140),center_color=(0,0,0)),
                          embeded_image_path="Logos/github-logo.png")

img_2.save("img/Danny_Goossen_Github_QR.png")


qr= qrcode.QRCode(error_correction=qrcode.constants.ERROR_CORRECT_H)
qr.add_data('https://gitlab.com/gioxa')

img_2 = qr.make_image(image_factory= StyledPilImage, module_drawer=RoundedModuleDrawer(),
                          color_mask=RadialGradiantColorMask(edge_color=(255,25,5),center_color=(0,0,0)),
                          embeded_image_path="Logos/gioxa-logo.png")

img_2.save("img/Danny_Goossen_Gioxa_QR.png")

qr= qrcode.QRCode(error_correction=qrcode.constants.ERROR_CORRECT_H)
qr.add_data('mailto:danny.goossen@gioxa.com')

img_2 = qr.make_image(image_factory= StyledPilImage, module_drawer=RoundedModuleDrawer(),
                          color_mask=RadialGradiantColorMask(edge_color=(0,173,255),center_color=(0,0,0)),
                          embeded_image_path="Logos/mail-logo.png")

img_2.save("img/Danny_Goossen_mailto_QR.png")

qr= qrcode.QRCode(error_correction=qrcode.constants.ERROR_CORRECT_H)
qr.add_data('+66894853610')

img_2 = qr.make_image(image_factory= StyledPilImage, module_drawer=RoundedModuleDrawer(),
                          color_mask=RadialGradiantColorMask(edge_color=(67,181,31),center_color=(0,0,0)),
                          embeded_image_path="Logos/telefoon-logo.png")

img_2.save("img/Danny_Goossen_call_QR.png")


qr= qrcode.QRCode(error_correction=qrcode.constants.ERROR_CORRECT_H)
qr.add_data('https://gitlab.com/dgoo2308/myqr')

img_2 = qr.make_image(image_factory= StyledPilImage, 
                          module_drawer=RoundedModuleDrawer(),
                          color_mask=RadialGradiantColorMask(edge_color=(255,131,49),center_color=(0,0,0)),
                          embeded_image_path="Logos/gitlab-logo.png")

img_2.save("img/Danny_Goossen_myqr_QR.png")

